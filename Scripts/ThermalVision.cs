using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThermalVision : MonoBehaviour
{

    public Shader shader;

    [Range(2f, 20f)]
    public float _ColorAmplification = 6.0f;

    private Material material;

    public delegate void OnThermalVisionEnabledDelegate(bool enabled);
    public static event OnThermalVisionEnabledDelegate onThermalVisionEnabled;

    // Use this for initialization
    void Start()
    {
        CheckForShader();
        CreateMaterial();
    }

    void OnEnable()
    {
        if (onThermalVisionEnabled != null) onThermalVisionEnabled(true);
    }

    void CheckForShader()
    {
        if (shader == null)
        {
            shader = Shader.Find("CVedia/ThermalVision");
        }

        if (!shader || !shader.isSupported)
        {
            this.enabled = false;
        }
    }

    void CreateMaterial()
    {
        material = new Material(shader);
        material.hideFlags = HideFlags.HideAndDontSave;
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        material.SetFloat("_ColorAmplification", _ColorAmplification);
        Graphics.Blit(source, destination, material);
    }

    protected virtual void OnDisable()
    {
        if (onThermalVisionEnabled != null) onThermalVisionEnabled(false);
    }
}
