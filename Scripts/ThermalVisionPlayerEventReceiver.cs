using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class ThermalVisionPlayerEventReceiver : MonoBehaviour
{
    public Color emissionColor = Color.yellow;
    public float emissionFactor = 5.0f;

    private List<Material> allMaterials = new List<Material>();
    private Renderer[] allRenderers;
    void Start()
    {
        allRenderers = transform.GetComponentsInChildren<Renderer>();

        foreach (Renderer item in allRenderers)
        {
            Material[] allMat = item.materials;
            allMaterials.AddRange(allMat);
        }
    }

    void OnEnable()
    {
        ThermalVision.onThermalVisionEnabled += OnThermalModeInit;
    }

    // Update is called once per frame
    void OnDisable()
    {
        ThermalVision.onThermalVisionEnabled -= OnThermalModeInit;
    }

    void OnThermalModeInit(bool enabled)
    {
        if (enabled)
        {
            foreach (Material m in allMaterials)
            {
                m.SetColor("_EmissionColor", emissionColor * emissionFactor);
            }
        }
        else
        {
            foreach (Material m in allMaterials)
            {
                m.SetColor("_EmissionColor", Color.black);
            }
        }
    }
}
