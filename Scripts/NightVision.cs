using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NightVision : MonoBehaviour
{

    public Shader shader;
    public Texture2D noiseTex;
    public Texture2D maskTex;

    [Range(0f, 1f)]
    public float _LuminanceThreshold = 0.3f;
    [Range(2f, 20f)]
    public float _ColorAmplification = 6.0f;
    [Range(0.1f, 1f)]
    public float _LightTreshold = 0.2f;
    [Range(0f, 8f)]
    public float _Zoom = 0f;

    private Material material;

    // Use this for initialization
    void Start()
    {
        CheckForShader();
        CreateMaterial();
    }

    void CheckForShader()
    {
        if (shader == null)
        {
            shader = Shader.Find("CVedia/NightVision");
        }

        if (!shader || !shader.isSupported)
        {
            this.enabled = false;
        }
    }

    void CreateMaterial()
    {
        material = new Material(shader);
        material.hideFlags = HideFlags.HideAndDontSave;
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        material.SetTexture("_NoiseTex", noiseTex);
        material.SetTexture("_MaskTex", maskTex);
        material.SetFloat("_LuminanceThreshold", _LuminanceThreshold);
        material.SetFloat("_ColorAmplification", _ColorAmplification);
        material.SetFloat("_LightTreshold", _LightTreshold);
        material.SetFloat("_Zoom", _Zoom);
        Graphics.Blit(source, destination, material);
    }
}
