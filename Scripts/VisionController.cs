﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionController : MonoBehaviour
{

    NightVision nightVision;
    ThermalVision thermalVision;
    EMVision emVision;

    public KeyCode normalVisionKey = KeyCode.Alpha1;
    public KeyCode normalVisionAlternateKey = KeyCode.Keypad1;

    public KeyCode nightVisionKey = KeyCode.Alpha2;
    public KeyCode nightVisionAlternateKey = KeyCode.Keypad2;

    public KeyCode thermalVisionKey = KeyCode.Alpha3;
    public KeyCode thermalVisionAlternateKey = KeyCode.Keypad3;

    public KeyCode emVisionKey = KeyCode.Alpha4;
    public KeyCode emVisionAlternateKey = KeyCode.Keypad4;

    // Use this for initialization
    void Start()
    {
        nightVision = FindObjectOfType<NightVision>();
        thermalVision = FindObjectOfType<ThermalVision>();
        emVision = FindObjectOfType<EMVision>();

        nightVision.enabled = false;
        thermalVision.enabled = false;
        emVision.enabled = false;
    }

    void DisableAllModes()
    {
        nightVision.enabled = false;
        thermalVision.enabled = false;
        emVision.enabled = false;
    }


    void Update()
    {
        if (Input.GetKeyDown(normalVisionKey) || Input.GetKeyDown(normalVisionAlternateKey))
        {
            DisableAllModes();
        }

        if (Input.GetKeyDown(nightVisionKey) || Input.GetKeyDown(nightVisionAlternateKey))
        {
            DisableAllModes();
            nightVision.enabled = true;
        }

        if (Input.GetKeyDown(thermalVisionKey) || Input.GetKeyDown(thermalVisionAlternateKey))
        {
            DisableAllModes();
            thermalVision.enabled = true;
        }

        if (Input.GetKeyDown(emVisionKey) || Input.GetKeyDown(emVisionAlternateKey))
        {
            DisableAllModes();
            emVision.enabled = true;
        }
    }
}
