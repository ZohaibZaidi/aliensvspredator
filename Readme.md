# CVedia test

### Todos
- Night Vision
- Thermal Vision
- EMP Vision


#### Night Vision
Reference links : 
- [Valve Shader Reference](https://developer.valvesoftware.com/wiki/Vision_Nocturna#Nightvision_with_shaders)
- [Geeks3d Shader Reference](http://www.geeks3d.com/20091009/shader-library-night-vision-post-processing-filter-glsl/)

#### Thermal Vision
Reference links :
- [Coding Experiments](http://coding-experiments.blogspot.com.tr/2010/10/thermal-vision-pixel-shader.html)


#### EMVision
Using the same thermal vision shader with a different color for the mesh

## How To Use
- Copy the Scripts, Shaders and Textures folders to your Unity3d project
- Assign your NightVision, ThermalVision, EMVision and VisionController scripts to the MainCamera
- Assign the corresponding shaders to the attached scripts. Thermal shader is used in both ThermalVision and EMVision
- NightVision.shader will be dragged to the NightVision component and so on. Even if not applied the script shall do so itself
- Drag the mask and noise texture to the NightVision script
- Attach ThermalVisionPlayerEventReceiver.cs to the parent object of the mesh that should be visible in this mode
- Attach EMVisionPlayerEventReceiver.cs to the parent object of the mesh that should be visible in this mode

## Remaining Features
- Thumbnail mode

## Future improvements
- Add a transition animation between mode toggles
- Add a better mask image to all modes
- Color grading needs work
- Camera movement
- Control lighting when using these modes
